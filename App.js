class Pendu {
    tryTest = 12;
    word = 'JavaScript';
    wordArray = this.word.split('');
    lettersNumber = this.wordArray.length;


    isIncluded(letter) {
        if (!this.wordArray.includes(letter)) {
            this.tryTest --;
            return this.tryTest;
        }
    }

    displayTryWord(letter) {
        let tryWord = [];
        for (let i = 0; i < this.lettersNumber; i++) {
            if (letter === this.wordArray[i]) {
                tryWord[i] = letter;
            } else {
                tryWord[i] = '_';
            }
        }

        console.log(tryWord);
        this.isIncluded(letter);
        console.log(this.isIncluded(letter));
    }
}

const newGame = new Pendu();

console.log(newGame.displayTryWord('a'));
console.log(newGame.displayTryWord('b'));